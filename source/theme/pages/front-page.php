<?php
/* Template Name: Front Page */
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>

  <main>
    <section class="section-intro bg-light pt-4">
      <div class="container">
        <div class="text-center pb-5"><a href="<?php home_url(); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/logo-virality.svg" alt="Virality Diagnostics logo" class="logo-virality"></a></div>
        <div class="row kit-container py-5">
            <h1 class="col-lg-10 col-xl-9 mb-4 pb-2">Health Diagnostics for a Rapidly Changing World</h1>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/splash-page/kit-box.png" alt="Rapid Covid-19 Test Kit" class="kit-in-box img-fluid mt-4 mb-n5">
            <div class="w-100"></div>
            <p class="col-lg-6">Virality’s mission is to improve the health and well-being of all of our customers, from first-responders and medical workers through home consumers. We do so by providing top-tier testing and diagnostics products in an environment where the importance of testing and diagnosis in disease monitoring and prevention is exponentially greater than it has ever been.</p>
        </div>
      </div>
    </section>

    <section class="section-kit">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 offset-lg-6 content-parent">
            <div class="position-relative mt-5 pt-3">
              <p class="h6">Now available</p>
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/logo-knowvid19.svg" alt="Kovid19 logo" class="my-4 pb-2">
              <h1 class="h2">Announcing Rapid Covid-19 IgG/IgM antibody Test Kit for Healthcare Professionals</h1>
              <p>Virality’s new on-site/in-home test provides healthcare workers and first responders (and those they serve) with the tools necessary to evaluate if they have or have had COVID-19, returning results in 15 minutes or less. This kit finally provides a mass-volume solution for improving awareness of risk of contracting the disease and may reduce the demand for personal protective equipment (PPE) for those exhibiting signs of immunity.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section-what-offer container text-center">
      <p class="h6">What we offer</p>
      <h1 class="mb-5 pb-5">Coming Soon</h1>
      <ul class="list-unstyled row">
        <li class="col-md-6 px-lg-5 col-xl-5 offset-xl-1 px-xl-4X">
          <img src="https://via.placeholder.com/540x300" class="product-image my-3" alt="">
          <h5 class="my-3">Rapid Covid-19 Antibody Test Kit for Corporate Buyers</h5>
          <div class="text-smallish">
            <p>Virality’s new on-site/in-home test provides business professionals with the tools necessary to affordably support their entire employee population. Evaluating whether employees have or have had COVID-19 with a simple 15 minute-or-less process (completed at the work-site or in-home) allows both the employer and employee to make better informed decisions related to risk of exposure and may reduce demand for personal protective equipment (PPE) for those exhibiting signs of immunity.</p>
          </div>
        </li>
        <li class="col-md-6 px-lg-5 col-xl-5 offset-xl-1 px-xl-4X">
          <img src="https://via.placeholder.com/540x300" class="product-image my-3" alt="">
          <h5 class="my-3">Rapid Covid-19 Antibody Test Kit for Consumers</h5>
          <div class="text-smallish">
            <p>Virality’s new in-home test provides any buyer with a simple kit to affordably evaluate whether they have or have had COVID-19. This simple 15 minute-or-less process allows any individual to make better informed decisions related to risk of exposure and may reduce demand for personal protective equipment (PPE) for those exhibiting signs of immunity.</p>
          </div>
      </ul>
    </section>

    <section class="section-our-strategy bg-primary text-light">
      <div class="container">
        <h5 class="h-intro">&mdash; Our strategy</h5>
        <h3 class="mb-5">Leading the Market with Healthcare Product Innovation?</h3>
        <div class="text-smallish">
          <p>The quality and safety of our products and processes are our top priority. Our ongoing development of high-value testing, research, and diagnostics technologies positions us to contribute to a healthy and sustainable society.</p>
          <p>We are constantly working to lead the market in innovative products and services for healthcare workers, business professionals, and general consumers. We are committed to innovations that meet the most rigorous, universally accepted standards. </p>
          <p>Today and going forward, we are working with precision and tireless effort to research, develop and commercialize innovative testing and diagnostic healthcare products to protect humanity and to do so with sustainable, readily available, best-in-class solutions.</p>
        </div>
      </div>
    </section>

    <section class="section-personnel container">
      <h1>Executive Leadership</h1>
      <ul class="list-unstyled personnel-list">
        <li class="row row-odd">
          <div class="col-md-6 portrait-col">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/portraits/david-spiegel.jpg" alt="Picture of David A. Spiegel" class="img-fluid portrait">
          </div>
          <div class="col-md-6 bio-col">
            <h2>David A. Spiegel, MD, PhD</h2>
            <h3 class="h6 mb-5">Chief Scientific Advisor</h3>
            <p>David Spiegel, MD, PhD is a Professor at Yale University with appointments in the departments of Chemistry and Pharmacology. He graduated Magna Cum Laude, with Highest Honors in Chemistry from Harvard University in 1995, having worked in the laboratory of Professor Yoshito Kishi. He then went on to pursue a combined MD/PhD degree at Yale University, where he worked in the laboratory of Professor John L. Wood, focusing on synthetic organic chemistry, and graduating in 2005. After a brief postdoctoral stint at the Broad Institute of Harvard and MIT under Professor Stuart L. Schreiber, Professor Spiegel started his independent academic career at Yale University in 2007. </p>
            <p>Professor Spiegel has co-authored over 40 peer-reviewed publications and has over a dozen patents. He is the Chief Scientific Advisor and co-founder of Kleo Pharmaceuticals. He has also served as a consultant for International Flavors and Fragrances, Novartis Institute for Biomedical Research, Bristol-Myers Squibb, and Pharmaseq.</p>
            <p>Professor Spiegel has been recognized for his achievements with various awards and honors, including the NIH Director’s New Innovator Award, the Department of Defense Era of Hope</p>
            <p>Scholar Award, the Ellison Medical Foundation New Scholar Award in Aging Research, the Novartis Early Career Award in Organic Chemistry, the Bill and Melinda Gates Foundation Grand Challenges Explorations Award, the Alfred P. Sloan Foundation Fellowship, and others.</p>
          </div>
        </li>

        <li class="row row-even">
          <div class="col-md-6 portrait-col">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/portraits/erik-gordon.jpg" alt="Picture of Erik H. Gordon" class="img-fluid portrait">
          </div>
          <div class="col-md-6 bio-col">
            <h2>Erik H. Gordon</h2>
            <h3 class="h6 mb-5">CEO and Co-Founder</h3>
            <p>Erik is the CEO of The Gordon Group, a personal private equity and venture capital investment fund. Erik previously served as a strategy management consultant for Bain & Company, Inc. in Boston and London. Erik graduated Harvard College with honors and serves on numerous for-profit and nonprofit boards.</p>
          </div>
        </li>

        <li class="row row-odd">
          <div class="col-md-6 portrait-col">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/portraits/chris-minteer.jpg" alt="Picture of Chris Minteer" class="img-fluid portrait">
          </div>
          <div class="col-md-6 bio-col">
            <h2>Chris Minteer</h2>
            <h3 class="h6 mb-5">President and Co-Founder</h3>
            <p>hris graduated Magna Cum Laude, earning Top Honors in Chemistry & Biochemistry from College of the Holy Cross, while working in the laboratory of Professor Kenneth Mills. He then went on to earn his MPhil in Translational Biomedical Research from the University of Cambridge, completing research at Cancer Research UK investigating combination drug therapy approaches in Pancreatic Ductal Adenocarcinoma. His past research experiences have spanned enzymology, thermoanalytical chemistry, cancer therapeutics, bioinformatics, experimental pathology and nanotechnology.</p>
            <p>Chris is also a serial entrepreneur and brings experience in consumer retail, eCommerce analytics, global distribution and diagnostic testing through his past and current ventures as co-founder of Royal Flagship LLC, Minteer Formulas LLC and ScoutX Labs Inc.</p>
          </div>
        </li>

        <li class="row row-even">
          <div class="col-md-6 portrait-col">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/portraits/nick-minteer.jpg" alt="Picture of Nicholas J Minteerr" class="img-fluid portrait">
          </div>
          <div class="col-md-6 bio-col">
            <h2>Nicholas J Minteer</h2>
            <h3 class="h6 mb-5">COO and Co-Founder</h3>
            <p>Nick has founded and led a number of retail startups and currently serves as the CEO and Co-Founder at Royal Flagship – a premier global retailer specializing in market expansion, data analytics and turn-key logistics with an emphasis on global distribution. Royal Flagship's inception was realized after developing a proprietary eCommerce algorithm used to optimize retail growth. To date, Royal Flagship has sold across 20+ platforms, in 6 countries and has analyzed over 600 million products. Nick holds an MSc with Distinction from University College London and serves as an integral partner to connect Virality Diagnostics with the world.</p>
          </div>
        </li>
      </ul>

    </section>

  </main>

<?php endwhile; else: endif; ?>
<?php get_footer();
