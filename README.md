# Setup (First Run)

## Install System Dependencies

1. [Docker](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
  - Once you install the app, make sure it is running and you are signed in. It has to be running anytime you want to work on the project.
1. [Node + NPM](https://nodejs.org/en/)
  - Currently tested up to Node 10.
1. (If you want auto-reloading) [LiveReload Chrome Extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei) 

## Install Project Dependencies

1. `cd` to project directory
1. `npm run firstrun`
  - It may ask for your system password as it should automatically add `127.0.0.1 viralitydiagnostics.vm` to your hosts file.
1. `http://viralitydiagnostics.vm/wp-admin` should pop up, and you can enter `wordpress / wordpress` for user/pass.
  - NOTE: This does not start the gulp process. Run `npm start` to do your normal development once firstrun is complete.
1. At this point you likely need a database file to import, and perhaps some license codes. Contact the Project Admin (admin@wondergiant.com) for these.

---

# Daily Workflow (Once first run is complete)
1. `npm start`

# When Done Working:
1. `npm stop`

---

**Other:**
- `npm run nuke` Removes build, and all dockers.
  - It will likely ask for your system password.

**Connect with Sequel Pro**
Host: 127.0.0.1
user: wordpress
password: wordpress
database: wordpress
port: 3306

---

# HELP

- Issue: "Node sass could not find a binding...`npm rebuild node-sass`
- Issue: "internalBinding is not defined". Run `npm install natives@1.1.6` https://github.com/gulpjs/gulp/issues/2246
- There is a file called '/source/wp-config.staging.php'. You can copy that to the staging server, 1 folder up from public_html. wp-config.php will be looking for that file there.
- If the plugins aren't installing correctly (Dropbox errors), delete them from /build/wp-content/plugins, and rerun `npm run firstrun`
- Pulled DB, now site is all jacked up: If you ever pull a remote database and the site is all jacked up, it's likely the SiteGround Cache plugin; sg-cachepress. You just have to go into `build/wp-content/plugins/sg-cachepress` and rename to `sg-cachepress-bak` or whatever to temporarily disable it. Then you can login, and later rename the plugin back to it's original name (i.e. we rename so it gets disabled by wordpress locally).
- Funky stuff: If you just migrated or the site isn't working as a whole, try removing any security plugins (like all-in-one-security) from the plugins folder temporarily. Sometimes the rules they inject cause newly migrated sites to have incorrect settings.