<?php
function get_featured_image( $size='post-thumbnail' ){
	$return = (get_the_post_thumbnail_url() === false ) ? 
		get_template_directory_uri() . '/images/featured-image.jpg' :
			get_the_post_thumbnail_url( $post, $size );
	return $return;
}