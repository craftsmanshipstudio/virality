<?php
require_once( ABSPATH . '../config.php' );

// If we're behind a proxy server and using HTTPS, we need to alert Wordpress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) $_SERVER['HTTPS'] = 'on';

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
if( file_exists('/var/lib/sec/wp-settings.php') ) @include_once('/var/lib/sec/wp-settings.php'); // Added by SiteGround WordPress management system