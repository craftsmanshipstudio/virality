'use strict';

// DEPENDENCIES
// - - - - - - - - - - - - - - -
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const del = require("del");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");
const sourcemaps = require("gulp-sourcemaps");
const uglify = require("gulp-uglify");
const sass = require("gulp-sass");
const browsersync = require("browser-sync");
const babel = require("babelify");
const browserify = require("browserify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");

// PATHS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const path = {
  src: "source/",
  theme: "build/wp-content/themes/viralitydiagnostics/"
};

// BROWSER SYNC
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function browserSync(done) {
  browsersync.init({
    // proxy: 'http://localhost:8000',
    // host: 'localhost:8000',
    proxy: "http://viralitydiagnostics.vm",
    host: "viralitydiagnostics.vm",
    open: "external",
    port: 8000,
    notify: false
  });
  done();
}

// CLEAN
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Cleans the build directory
function clean() {
  return del(path.theme);
}

// PHP
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const phpPaths = {
  src: "source/theme/**/*.php",
  build: path.theme
};

function php() {
  return gulp
    .src(phpPaths.src)
    .pipe(newer(phpPaths.build))
    .pipe(gulp.dest(phpPaths.build))
    .pipe(browsersync.stream());
}

// IMAGES
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const imagePaths = {
  src: "source/theme/images/**/*",
  build: path.theme + "images/"
};

function images() {
  return gulp
    .src(imagePaths.src)
    .pipe(newer(imagePaths.build))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.svgo({
          plugins: [
            { optimizationLevel: 3 },
            { progessive: true },
            { interlaced: true },
            { removeViewBox: false },
            { removeUselessStrokeAndFill: false },
            { cleanupIDs: false }
          ]
        })
      ])
    )
    .pipe(gulp.dest(imagePaths.build))
    .pipe(browsersync.stream());
}

// STYLES
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const stylePaths = {
  app: "source/theme/styles/style.scss",
  login: "source/theme/styles/style-login.scss",
  admin: "source/theme/styles/style-admin.scss"
};

function stylesApp() {
  return gulp
    .src(stylePaths.app)
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: [],
        outputStyle: "compressed" // compact, compressed
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(concat("style.css"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(path.theme))
    .pipe(browsersync.stream());
}

// Login
function stylesLogin() {
  return gulp
    .src(stylePaths.login)
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: [],
        outputStyle: "compressed" // compact, compressed
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(concat("style-login.css"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(path.theme))
    .pipe(browsersync.stream());
}

// Admin
// function stylesAdmin() {
//   return gulp
//     .src(stylePaths.admin)
//     .pipe(sourcemaps.init())
//     .pipe(
//       sass({
//         includePaths: [],
//         outputStyle: 'compressed' // compact, compressed
//       }).on('error', sass.logError)
//     )
//     .pipe(
//       autoprefixer({
//         browsers: ['last 2 versions'],
//         cascade: false
//       })
//     )
//     .pipe(concat('style-admin.css'))
//     .pipe(sourcemaps.write('.'))
//     .pipe(gulp.dest(path.theme))
//     .pipe(browsersync.stream());
// }

// JAVASCRIPT
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var jsVendorPaths = [
  'source/theme/js/vendor/greensock/TweenMax.js',
  // 'source/theme/js/vendor/greensock/TimelineMax.js',
  // 'source/theme/js/vendor/greensock/utils/SplitText.js',
  // 'source/theme/js/vendor/plugins/DrawSVGPlugin.js',
  // 'source/theme/js/vendor/plugins/ScrollToPlugin.js',
  // 'source/theme/js/vendor/ScrollMagic.js',
  // "source/theme/js/vendor/bootstrap/bootstrap.bundle.min.js"
  // 'source/theme/js/vendor/flickity.pkgd.min.js',
  // 'source/theme/js/vendor/isotope.js',
];

function jsVendor() {
  return gulp
    .src(jsVendorPaths)
    .pipe(uglify().on("error", console.error))
    .pipe(concat("vendor.js"))
    .pipe(gulp.dest(path.theme + "/js/"))
    .pipe(browsersync.stream());
}

var jsCustomPaths = ["source/theme/js/app.js"];

function jsCustom() {
  return (
    browserify({ entries: jsCustomPaths })
      .transform(babel, { presets: ["@babel/preset-env"] })
      .bundle()
      .pipe(source("app.js"))
      .pipe(buffer())
      // .pipe( stripdebug() )
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(uglify().on("error", console.error))
      .pipe(sourcemaps.write("./"))
      .pipe(gulp.dest(path.theme + "/js"))
      .pipe(browsersync.stream())
  );
}

// COPY
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const itemsToCopy = [
  // static theme files
  {
    src: ["source/theme/screenshot.png"],
    dest: path.theme
  },
  // static base files
  {
    src: [
      "source/humans.txt",
      "source/robots.txt",
      "source/wp-config.php",
      "source/.htaccess"
    ],
    dest: "build/"
  },
  // fonts
  {
    src: ["source/theme/fonts/**/*"],
    dest: path.theme + "fonts/"
  },
  // icons
  {
    src: ["source/app-icons/**/*"],
    dest: path.theme + "/"
  }
];

gulp.task("copy", done => {
  itemsToCopy.forEach(obj => {
    gulp.src(obj.src).pipe(gulp.dest(obj.dest));
    // .pipe(browsersync.stream());
  });
  done();
});

// WATCH
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function watchFiles() {
  gulp.watch("source/theme/**/*.php", php);
  gulp.watch("source/theme/images/**/*", images);
  gulp.watch("source/theme/styles/**/*", styles);
  gulp.watch("source/theme/js/**/*", js);
}

// TASKS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const styles = gulp.parallel(stylesApp, stylesLogin);
const js = gulp.series(jsVendor, jsCustom);
const build = gulp.series(
  clean,
  gulp.parallel(php, images, styles, js, "copy")
);
const watchfiles = gulp.series(watchFiles);
const watch = gulp.parallel(watchFiles, browserSync);

// EXPORTS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
exports.build = build;
exports.watchfiles = watchfiles;
exports.watch = watch;