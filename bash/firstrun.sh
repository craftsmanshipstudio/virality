#!/usr/bin/env bash
GREEN='\033[32m'
NC='\033[0m' # No Color

docker-compose up -d &&
npm i &&
echo 'Waiting for Docker to finish...' &&
sleep 3 &&
echo 'Resuming...' &&
docker exec viralitydiagnosticswp sh -c 'curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar &&
  chmod +x wp-cli.phar &&
  mv wp-cli.phar /usr/local/bin/wp &&
  wp --info &&
  cd /var/www/html &&
  wp core install --allow-root --url=viralitydiagnostics.vm --title=Wordpress --admin_user=wordpress --admin_password=wordpress --admin_email=wordpress@wondergiant.com --skip-email
  ' &&
gulp build &&
bash ./bash/plugins.sh &&
# cp source/database.sql build/database.sql &&
# docker exec viralitydiagnosticswp sh -c 'wp db import database.sql --allow-root' &&
# rm -rf ./build/database.sql &&
# docker exec viralitydiagnosticswp sh -c 'wp login as wordpress --allow-root --url-only' &&
echo -e ${GREEN} '------------------------------------------------------------\n Next Step(s):\n 1. http://viralitydiagnostics.vm/wp-admin \n 2. wordpress/wordpress (to log in if this is your first setup)\n 3. Setup WP Migrate DB (instructions in README.md), and Pull.\n 4. npm start\n ------------------------------------------------------------\n' ${NC}
# echo $magic_url
# bash ./bash/login.sh
open http://viralitydiagnostics.vm/wp-admin/plugins.php
