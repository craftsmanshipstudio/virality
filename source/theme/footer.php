<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package custom-theme
 */
?>

<footer id="primary-footer" class="mt-5">
  <div class="container text-center">
    <ul class="row list-unstyled align-items-center">
      <li class="col small">Copyright ©2020 Virality Diagnostics</li>
      <li class="col text-center"><a href="http://viralitydiagnostics.com/" target="_blank"><img src="/wp-content/themes/viralitydiagnostics/images/logos/logo-virality.svg" alt="Virality Diagnostics logo" class="logo-virality"></a></li>
      <li class="col text-right"><a href="#" class="btn btn-primary rounded-pill px-4 text-uppercase font-weight-bold text-nowrap" role="button">Contact us</a></li>
    </ul>
    <ul>

  </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
