docker exec viralitydiagnosticswp sh -c '
  # Comment out any plugins you dont need.
  # For some reason we need the --allow-root param for this to work locally
  # Add --activate param for any plugins you want to be enabled by default
  # Try to keep this list alphabetical for sanity sake.

  wp plugin install https://www.dropbox.com/s/cpplsopdda8jud8/advanced-custom-fields-pro.zip?dl=1 --allow-root --activate #Premium
  wp plugin install all-in-one-wp-security-and-firewall --allow-root --activate
  wp plugin install aryo-activity-log --allow-root --activate
  wp plugin install password-protected --allow-root --activate
  wp plugin install redirection --allow-root --activate
  # wp plugin install https://www.dropbox.com/s/rnd5uavonhe31lz/sg-cachepress.zip?dl=1 --allow-root --activate
  wp plugin install updraftplus --allow-root --activate
  wp plugin install wp-fail2ban --allow-root --activate
  wp plugin install https://www.dropbox.com/s/mvvgl04priuda8l/wp-migrate-db-pro.zip?dl=1 --allow-root --activate #Premium
  wp plugin install https://www.dropbox.com/s/5uwahqe6i0hfuo4/wp-migrate-db-pro-cli.zip?dl=1 --allow-root --activate #Premium
  wp plugin install https://www.dropbox.com/s/6blqpf66ert9rxv/wp-migrate-db-pro-media-files.zip?dl=1 --allow-root --activate #Premium
  wp plugin install https://www.dropbox.com/s/2a7g0ciy254sr0q/wp-migrate-db-pro-theme-plugin-files.zip?dl=1 --allow-root --activate #Premium
  wp plugin install wordpress-seo --allow-root --activate # Yoast SEO

  # Delete default plugins
  wp plugin delete hello --allow-root

  # Delete some default themes
  wp theme activate viralitydiagnostics --allow-root

  wp theme delete twentynineteen --allow-root
  wp theme delete twentyseventeen --allow-root
  wp theme delete twentysixteen --allow-root
  wp theme delete twentytwenty --allow-root

  wp login install --allow-root --activate
  '
  echo -e ${GREEN} '------------------------------------------------------------\n Next Step(s):\n 1. http://viralitydiagnostics/wp-admin \n 2. wordpress/wordpress (to log in if this is your first setup)\n 3. Setup WP Migrate DB (instructions in README.md), and Pull.\n 4. npm start\n ------------------------------------------------------------\n' ${NC}
